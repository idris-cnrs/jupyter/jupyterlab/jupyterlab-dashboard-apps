# Jupyterlab Apps Launcher

This project is a jupyterlab extension to add launcher icons of Dask and Ray dashboards
in Dashboards category. These launcher icons open an input dialog for the user to
provide the port where the dashboards are running and open those dashboards via
JupyterLab server proxy.

## Installation

To install the extension

```
pip install git+https://gitlab.com/idris-cnrs/jupyter/jupyterlab/jupyterlab-dashboard-apps.git
```

## Developmental install

To setup a developmental install

```
git clone https://gitlab.com/idris-cnrs/jupyter/jupyterlab/jupyterlab-dashboard-apps.git
cd jupyterlab-documentation-icons
pip install -e .
```

You can watch the source directory and run JupyterLab at the same time in different terminals to watch for changes in the extension's source and automatically rebuild the extension.

```
# Watch the source directory in one terminal, automatically rebuilding when needed
jlpm run watch
# Run JupyterLab in another terminal
jupyter lab
```

With the watch command running, every saved change will immediately be built locally and available in your running JupyterLab. Refresh JupyterLab to load the change in your browser (you may need to wait several seconds for the extension to be rebuilt).

