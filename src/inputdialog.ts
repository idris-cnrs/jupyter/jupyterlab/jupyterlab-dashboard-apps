// Copyright 2022 IDRIS / jupyter
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Widget } from '@lumino/widgets';

/**
 * Widget for inputing dashboard port number
 */
export class DashboardAppArgsWidget extends Widget {
  constructor(placeholder: string) {
    super({ node: Private.createOpenNode(placeholder) });
  }

  /**
   * Get the value of the widget
   */
  getValue(): string {
    return this.inputNode.value;
  }

  /**
   * Get the input text node.
   */
  get inputNode(): HTMLInputElement {
    return this.node.getElementsByTagName('input')[0] as HTMLInputElement;
  }
}

namespace Private {
  export function createOpenNode(placeholder: string): HTMLElement {
    const body = document.createElement('div');
    const portLabel = document.createElement('label');
    portLabel.textContent = 'Dashboard running at port:';

    const port = document.createElement('input');
    port.value = '';
    port.placeholder = placeholder;

    body.appendChild(portLabel);
    body.appendChild(port);
    return body;
  }

  export const id = 0;
}
