// Copyright 2022 IDRIS / jupyter
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';
import { ILauncher } from '@jupyterlab/launcher';
import { PageConfig } from '@jupyterlab/coreutils';
import {
  IFrame,
  MainAreaWidget,
  WidgetTracker,
  showDialog,
  Dialog,
  showErrorMessage
} from '@jupyterlab/apputils';
import { ISettingRegistry } from '@jupyterlab/settingregistry';
import { LabIcon } from '@jupyterlab/ui-components';
import { DashboardAppArgsWidget } from './inputdialog';
import daskAppSvgStr from '../style/icons/dask.svg';
import rayAppSvgStr from '../style/icons/ray.svg';

const EXTENSION_NAME = '@idriscnrs/dashboard-apps:category';

export const daskIcon = new LabIcon({
  name: 'dashboardapps:daskIcon',
  svgstr: daskAppSvgStr
});

export const rayIcon = new LabIcon({
  name: 'dashboardapps:rayIcon',
  svgstr: rayAppSvgStr
});

function newDashboardAppWidget(
  id: string,
  url: string,
  text: string
): MainAreaWidget<IFrame> {
  const content = new IFrame({
    sandbox: [
      'allow-same-origin',
      'allow-scripts',
      'allow-popups',
      'allow-forms'
    ]
  });
  content.title.label = text;
  content.title.closable = true;
  content.url = url;
  content.addClass('jp-DashboardsApp');
  content.id = id;
  const widget = new MainAreaWidget({ content });
  widget.addClass('jp-DashboardsApp');
  return widget;
}

async function isNumber(port: string): Promise<string> {
  if (port !== null && port !== '' && !isNaN(Number(port))) {
    return port;
  } else {
    throw new Error(`Port ${port} is not a valid number`);
  }
}

async function activate(
  app: JupyterFrontEnd,
  launcher: ILauncher,
  settingRegistry: ISettingRegistry | null
): Promise<void> {
  console.log('@idriscnrs/dashboard-apps - extension is activated!');

  let category = 'Dashboards';
  if (settingRegistry) {
    const settings = await settingRegistry.load(EXTENSION_NAME);
    category = settings.get('category').composite as string;
  }

  const { commands, shell } = app;

  const namespace = 'dashboard-apps';
  const tracker = new WidgetTracker<MainAreaWidget<IFrame>>({
    namespace
  });
  const command = namespace + ':' + 'open';

  const dashboardApps = [
    {
      name: 'dask',
      title: 'Dask',
      url: '/status',
      defPort: '8787',
      newBrowserTab: true,
      enabled: true,
      icon: daskIcon
    },
    {
      name: 'ray',
      title: 'Ray',
      url: '/',
      defPort: '8265',
      newBrowserTab: true,
      enabled: true,
      icon: rayIcon
    }
  ];

  commands.addCommand(command, {
    label: (args: any) => args['title'] as string,
    icon: (args: any) => args['icon'],
    execute: (args: any) => {
      const id = args['id'] as string;
      const name = args['name'] as string;
      const title = args['title'] as string;
      const url = args['url'] as string;
      const defPort = args['defPort'] as string;
      const newBrowserTab = args['newBrowserTab'] as boolean;
      showDialog({
        title: 'Open ' + name + ' Dashboard',
        body: new DashboardAppArgsWidget(defPort),
        buttons: [Dialog.cancelButton(), Dialog.okButton({ label: 'Launch' })],
        focusNodeSelector: 'inpute'
      }).then((result: any) => {
        if (result.button.label === 'Launch') {
          const input = <string>result.value;
          isNumber(input)
            .then(port => {
              const full_url = PageConfig.getBaseUrl() + `proxy/${port}` + url;
              if (newBrowserTab) {
                window.open(full_url, '_blank');
                return;
              }
              let widget = tracker.find((widget: any) => {
                return widget.content.id === id;
              });
              if (!widget) {
                widget = newDashboardAppWidget(id, full_url, title);
              }
              if (!tracker.has(widget)) {
                void tracker.add(widget);
              }
              if (!widget.isAttached) {
                shell.add(widget);
                return widget;
              } else {
                shell.activateById(widget.id);
              }
            })
            .catch(err => showErrorMessage('Invalid port number', err));
        }
      });
    }
  });

  for (const app of dashboardApps) {
    if (!app.enabled) {
      continue;
    }

    const url = app.url;
    const defPort = app.defPort;
    const title = app.title;
    const newBrowserTab = app.newBrowserTab;
    const id = namespace + ':' + app.name;
    const appIcon = app.icon;
    const launcher_item: {
      args: {
        icon: any;
        newBrowserTab: boolean;
        id: string;
        title: string;
        name: string;
        url: string;
        defPort: string;
      };
      category: string;
      command: string;
    } = {
      command: command,
      args: {
        url: url,
        defPort: defPort,
        name: title,
        title: title + (newBrowserTab ? ' [↗]' : ''),
        newBrowserTab: newBrowserTab,
        id: id,
        icon: appIcon
      },
      category: category
    };
    launcher.add(launcher_item);
  }
  return;
}

/**
 * Initialization data for the jupyterlab-server-proxy extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: EXTENSION_NAME,
  autoStart: true,
  requires: [ILauncher],
  optional: [ISettingRegistry],
  activate: activate
};

export default extension;
